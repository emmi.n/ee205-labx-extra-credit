///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file utilities.cpp
/// @version 1.0
///
/// a file for both the ABSTRACT_TEST_CLASS and Results class
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   12_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <string_view>
#include <cstring>

#include "utilities.hpp"

using namespace std;

////// @note methods for Result class //////
Result::Result( ABSTRACT_TEST_CLASS* pNewTestClass, string_view newStructureName ) {  // constructor
   clear();
	pTestClass = pNewTestClass;
	structure  = newStructureName;
}

const string_view Result::getStructure() const { return structure; }

void Result::clear() {
	structure = "";
	memset( insert_ticks, 0, sizeof( insert_ticks) );
	memset( clear_ticks,  0, sizeof( clear_ticks) );
	memset( search_ticks, 0, sizeof( search_ticks) );
}

void Result::recordInsertTicks( size_t testIndex, size_t runIndex, ticks_t result ) {
	if( runIndex >= THROWAWAY_RUNS ) {
		insert_ticks[testIndex]  += result;
	}
}

void Result::recordClearTicks( size_t testIndex, size_t runIndex, ticks_t result ) {
	if( runIndex >= THROWAWAY_RUNS ) {
		clear_ticks[testIndex]  += result;
	}
}

void Result::recordSearchTicks( size_t testIndex, size_t runIndex, ticks_t result ) {
	if( runIndex >= THROWAWAY_RUNS ) {
		search_ticks[testIndex]  += result;
	}
}

void Result::printResultsHeader() {
	printf( "%21s", " " );
	for( unsigned int test = 0 ; test < TESTS ; test++ ) {
		printf( "%13u ", TEST_SIZE[test] );
	}
	printf( "\n" );

	printf( "==================== " );
	for( unsigned int test = 0 ; test < TESTS ; test++ ) {
		printf( "============= " );
	}
	printf( "\n" );
}

void Result::printResults() const {
	printf( "%-20s", (structure + " insert").c_str() );

	for( unsigned int test = 0 ; test < TESTS ; test++ ) {
		ticks_t average = insert_ticks[test] / ( TEST_RUNS * TEST_SIZE[test] );
		printf( "%14lu", average );
	}
	printf( "\n" );

	printf( "%-20s", (structure + " search").c_str() );

	for( unsigned int test = 0 ; test < TESTS ; test++ ) {
		ticks_t average = search_ticks[test] / ( TEST_RUNS * SEARCH_TESTS );
		printf( "%14lu", average );
	}
	printf( "\n" );


	printf( "%-20s", (structure + " clear").c_str() );

	for( unsigned int test = 0 ; test < TESTS ; test++ ) {
		ticks_t average = clear_ticks[test] / ( TEST_RUNS );
		printf( "%14lu", average );
	}
	printf( "\n" );

	printf( "- - - - - - - - - - \n" );
}

void Result::testDataStructure() {
	for( size_t testIndex = 0 ; testIndex < TESTS ; testIndex++ ) {
   	unsigned int testSize = TEST_SIZE[testIndex];  // Store locally, so we aren't always computing it

		for( size_t run = 0 ; run < THROWAWAY_RUNS + TEST_RUNS ; run++ ) {
			// Setup the test infrastructure
			ticks_t accumulator = 0;

			// Setup the test //////
			////////////////////////
			pTestClass->initDataStructure();

			// Insert the data
			for( size_t loop = 0 ; loop < testSize ; loop++ ) {
				accumulator += pTestClass->testInsert();
			}

			// Record the inserts
			recordInsertTicks( testIndex, run, accumulator );

			// Search the data
			accumulator = 0;
			for( size_t loop = 0 ; loop < SEARCH_TESTS ; loop++ ) {
				accumulator += pTestClass->testSearch();
			}
			recordSearchTicks( testIndex, run, accumulator );

			// Clear the data structure and record the results
			recordClearTicks( testIndex, run, pTestClass->clearDataStructure() );

#ifdef PRINT_PROGRESS
			// Print progress
			cout << ".";
			cout.flush();
#endif
		} // testIndex : TESTS

#ifdef PRINT_PROGRESS
	// Print progress
	cout << " >> ";
	cout.flush();
#endif

	} // run : RUNS
} // testDataStructure()

