///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file utilities.hpp
/// @version 1.0
///
/// a file for both the ABSTRACT_TEST_CLASS and Results class
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   12_MAY_2021
///////////////////////////////////////////////////////////////////////////////

/// @note This abstract test class is a template for concrete classes that can
/// actually do tests.

#pragma once

#include <string>
#include <string_view>

#include "globals.hpp"
#include "utilities.hpp"

class ABSTRACT_TEST_CLASS {
protected:
	static long start, end;
	static int  insert_value;
	static int  search_value;

public:
	virtual inline void    initDataStructure()  __attribute__((always_inline)) = 0;
	virtual inline ticks_t testInsert()         __attribute__((always_inline)) = 0;
	virtual inline ticks_t clearDataStructure() __attribute__((always_inline)) = 0;
	virtual inline ticks_t testSearch()         __attribute__((always_inline)) = 0;
};

/// @note A class to print out all the results to the command line 
class Result {
private:
	ABSTRACT_TEST_CLASS* pTestClass = nullptr;

   std::string structure = "";
	ticks_t insert_ticks[TESTS];                       // Sum of all ticks
	ticks_t clear_ticks[TESTS];                        // Sum of all ticks
	ticks_t search_ticks[TESTS];                       // Sum of search ticks

public:
	Result( ABSTRACT_TEST_CLASS* pNewTestClass, std::string_view newStructureName );

	const std::string_view getStructure() const;
	void  clear();

	void recordInsertTicks( size_t testIndex, size_t runIndex, ticks_t result );
	void recordClearTicks ( size_t testIndex, size_t runIndex, ticks_t result );
   void recordSearchTicks( size_t testIndex, size_t runIndex, ticks_t result );
	
   void printResultsHeader();
	void printResults() const;
	
   void testDataStructure();
} ;
