///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file eval.cpp
/// @version 1.0
///
/// Evaluate a number of Standard C++ Collection classes for performance
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   12_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <cstring>
#include <string>
#include <cmath>
#include <x86intrin.h>
#include <limits.h>

#include "globals.hpp"

using namespace std;

// Inline assembly routine that returns high percision monotonic clock
// ticks from the CPU
#define MARK_TIME(ticks)  asm inline volatile (               \
                          "RDTSCP\n\t"                        \
                          "SHL $32,   %%rdx\n\t"              \
                          "OR  %%rax, %%rdx\n\t"              \
                          "MOV %%rdx, %0\n\t"                 \
                          : "=r" (ticks)                      \
                          :                                   \
                          : "%rax", "%rcx", "%rdx")           ;

long TEST_OVERHEAD = LONG_MAX;

// Compute the number of ticks between start and end, subtracting the overhead.
// If it's negative, then it's 0.
#define DIFF_TIME(start, end) ((end-start-TEST_OVERHEAD > 0) ? (end-start-TEST_OVERHEAD) : 0)



///////////////////////////////////  main  ///////////////////////////////////
int main() {
	cout << "Welcome to the Gnu C++ Collection Class Evaluator" << endl;

	static long start, end;
	for( unsigned int i = 0 ; i < CALIBRATE_OVERHEAD ; i++ ) {
		MARK_TIME( start );
		MARK_TIME( end );
		TEST_OVERHEAD = min( TEST_OVERHEAD, (end - start) );
	}

	cout << "Approximate test overhead is: " << TEST_OVERHEAD << endl;

	// Implement your Collection Class Evaluator here

} // main()
