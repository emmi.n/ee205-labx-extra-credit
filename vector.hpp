///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file vector.hpp
/// @version 1.0
///
/// a class that wraps around a vector container
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   12_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include<random>
#include<vector>

// @note This class wraps a vector<long> container
class TestVector : public ABSTRACT_TEST_CLASS {
private:
	static vector<long> container;

public:
   // initilize the vector
	virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

   // Insert data into the vector (measuring the insertion time)
	virtual inline ticks_t testInsert() __attribute__((always_inline)) {
		auto endIterator = container.end();
		auto insert_value = rand();

		MARK_TIME( start );

		// Do the operation //
		container.insert( endIterator, insert_value );
		//////////////////////

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

   // Clear data out of the vector
	virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
		MARK_TIME( start );

		// Do the operation //
		container.clear();

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

   // Search for data in the vector
	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

		MARK_TIME( start );

		// Do the operation //
		for( auto i : container ) {
			if( i == search_value )
				break;
		}

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

};
