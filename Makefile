###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 10x - Collection Class Evaluator
#
# @file    Makefile
# @version 1.0
#
# @author EmilyPham <emilyn3@hawaii.edu>
# @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
# @date   12_MAY_2021
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

AR       = ar

TARGETS  = eval

all: $(TARGETS)

utilities.o: utilities.cpp globals.hpp utilities.hpp
	$(CXX) -c $(CXXFLAGS) $<

libcontainers.a: utilities.o
	$(AR) -rsv $@ $^


$(TARGETS): %: %.cpp libcontainers.a
#	$(CXX) $(CXXFLAGS) -S -fverbose-asm $^
	$(CXX) $(CXXFLAGS) -o $@ $^

clean:
	rm -f *.o *.a $(TARGETS)
