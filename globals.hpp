///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file globals.hpp
/// @version 1.0
///
/// Evaluate a number of Standard C++ Collection classes for performance
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   12_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

typedef unsigned long ticks_t;

constexpr unsigned int TESTS              = 5;    // The number of elements in TEST_SIZE[]
constexpr unsigned int TEST_SIZE[TESTS]   = { 10, 100, 1000, 10000, 100000 };
constexpr unsigned int THROWAWAY_RUNS     = 2;    // Unused runs that initialize memory
constexpr unsigned int TEST_RUNS          = 10;   // Measured test runs
constexpr unsigned int SEARCH_TESTS       = 100;  // Number of times we try searching the data structure
constexpr unsigned int CALIBRATE_OVERHEAD = 1000; // Number of times to calibrate the test overhead
